const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const sqlite3 = require('sqlite3').verbose();

let db = new sqlite3.Database('./quiz-db/database/quizzesdata.db', sqlite3.OPEN_READWRITE, (err) => {
  if (err) {
    return console.error(err.message);
  }
  console.log('Connected to in-memory SQLite3 database.');
})

app.get('/', function(req, res){
  res.sendfile('index.html');
});

app.post('/makerequest', function(request, response) {

    console.log("Got response: " + response.statusCode);
});

io.on('connection', function(socket){
  console.log('a user connected');

  //var question_type = '--'
  var question_text = '--'
  var correct_answer = '--'

  // retrieve question based on user id, for now assume 1
  db.serialize(() => {
    db.each(`select * from quizzes 
      inner join users on users.group_id = quizzes.group_id
      inner join questions on questions.quiz_id = quizzes.quiz_id
      where quizzes.quiz_id = 1`, (err, row) => {
        if (err) {
            console.error(err.message);
        }
        console.log(row.user_id + "\t" + row.username + "\t" + row.quiz_name + "\t" + row.question_text + "\t" + row.correct_answer)
        //question_type = row.qtype;
        question_text = row.question_text;
        correct_answer = row.correct_answer;
        // emit question
        socket.emit('question', {
          qtype: 'short-answer',
          question: question_text,
          correct: correct_answer
        })
      });
  });

  socket.on('answer', function (answer, correct) {
  	if (answer != null && answer.answer == answer.correct)
  	{
  		io.emit('correct');
  		console.log('Correct. Submitted: ', answer.answer, '\nCorrect!');
  	}
  	else
  	{
  		io.emit('incorrect', {correct: correct})
  		console.log('Incorrect. Submitted: ', answer.answer, '\nActual: ', answer.correct);
  	}
  });

  socket.on('unsecurelogin', function (email, password) {
    if (email.email != null && email.password == "FakeTestPassword")
    {
      io.emit('correctlogin');
      console.log('Logged in as ' + email.email);
    }
    else
    {
      io.emit('incorrectlogin')
      console.log('Incorrect.');
      console.log(email.email);
      console.log(email.password);
    }
  });

  socket.on('setlocation', function (lat, lon) {
    db.serialize(() => {
      db.run('create table if not exists locations (latitude real not null,longitude real not null);');

      db.run('INSERT INTO locations VALUES (' + lat.lat + ',' + lat.lon + ');');
      
      console.log ("inserted into table " + lat.lat + ", " + lat.lon);
    });
    

  });

});

setInterval(() => {
  io.emit('ping', { data: (new Date())/1});
}, 1000);

http.listen(3000, function(){
  console.log('listening on *:3000');
});

function send_updated_question(question_type, question_text, correct_answer)
{
  socket.emit('question', {
    qtype: question_type,
    question: question_text,
    correct: correct_answer
  })
}
