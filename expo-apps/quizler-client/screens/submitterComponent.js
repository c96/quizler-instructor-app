import React from 'react';
import {
  Platform,
  Text,
  TextInput,
} from 'react-native';
import { Constants } from 'expo';

const io = require('socket.io-client');

// Local ngrok URL for websockets
const SocketEndpoint = 'http://e9d8443a.ngrok.io';

export default class submitterComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      answer: "testanswer",
      answeredCorrect: false,
      active: false
    };
  }

  componentDidMount() {

    if (active) {
      // web socket ping
      const socket = io(SocketEndpoint, {
        transports: ['websocket'],
      });

      socket.emit('answer', {answer: this.state.answer});

      socket.on('correct', () => {
        this.setState({answeredCorrect: true});
      });
      socket.on('incorrect', (correct) => {
        this.setState({answeredCorrect: false});
      });
    }
    
  }
}