import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { Constants, Location, Permissions, WebBrowser } from 'expo';

const io = require('socket.io-client');

import { MonoText } from '../components/StyledText';

// Local ngrok URL for websockets

const SocketEndpoint = 'http://e9d8443a.ngrok.io';

var difference = function (a, b) { 
  return Math.abs(a - b); 
}

export default class InfoScreen extends React.Component {
  state = {
    isConnected: false,
    data: null,
    desiredLatitude: null,
    desiredLongitude: null
  };
  componentDidMount() {

    // web socket ping
    const socket = io(SocketEndpoint, {
      transports: ['websocket'],
    });

    socket.on('connect', () => {
      this.setState({ isConnected: true });
    });

    socket.on('ping', data => {
      this.setState(data);
    });

    socket.on('locate', data => {
      this.setState(desiredLatitude);
      this.setState(desiredLongitude);
    })

    // Android emulator check
    // Get location
    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({
        errorMessage: 'Error occurred, please try on Android device instead of emulator!',
      });
    } else {
      this._getLocationAsync();
    }
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    this.setState({ location });
  };

  render() {

    let locText = 'Waiting..';
    let locStatus = 'Waiting..';

    let latValue = 0;
    let lonValue = 0;

    // Default coordinates
    let defaultLat = 37.785;
    let defaultLon = -122.41;

    // Check location and set
    if (this.state.errorMessage) {
      locText = this.state.errorMessage;
      locStatus = this.state.errorMessage;
    } else if (this.state.location) {
      latValue = this.state.location.coords.latitude;
      lonValue = this.state.location.coords.longitude;

      locText = // JSON.stringify(this.state.location) +
      "\nLatitude: " + latValue 
      + "\nLongitude: " + lonValue;

      // check if attending through difference with default coords
      if (difference(latValue,defaultLat) < 1
        && difference(lonValue,defaultLon) < 1)
      {
        locStatus = "Location is close enough";
      }
      else {
        locStatus = "Location is not close enough"
      }
    }

    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1, backgroundColor: '#605ca8'}} >
          <Image
            style={{flex: 1}}
            source={require('../assets/images/logo.png')}
            resizeMethod="resize"
            resizeMode="center"
          />
        </View>
        <View style={{flex: 5, padding: 20}}>
          <View style={{flex: 2, backgroundColor: '#ecf0f5', padding: 20}} >
            <Text>Connecting to Quizler...</Text>
            <Text style={styles.paragraph}>connected: {this.state.isConnected ? 'true' : 'false'}</Text>
            {this.state.data &&
              <Text>
                ping response: {this.state.data}
              </Text>}
          </View>
          <View style={{flex: 8, padding: 20, backgroundColor: '#f4f4f4'}} >
          <Text>Attendance Check</Text>
            <View style={{flex: 2, padding: 10, margin: 15, backgroundColor: '#e0e0f5'}}> 
            <Text>Location:</Text>
            <Text style={styles.paragraph}>{locText}</Text>
            </View>
            <View style={{flex: 2, padding: 10, margin: 15, backgroundColor: '#e0e0f5'}}>
            <Text>Attending:</Text>
            <Text style={styles.paragraph}>{locStatus}</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    textAlign: 'center',
  },
});