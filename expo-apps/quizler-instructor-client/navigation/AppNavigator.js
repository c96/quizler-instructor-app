import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  Button,
  StatusBar,
  StyleSheet,
  View,
  Text,
  TextInput,
} from 'react-native';
import { createStackNavigator, createSwitchNavigator, createAppContainer } from 'react-navigation';
import MainTabNavigator from './MainTabNavigator';

const io = require('socket.io-client');

// Local ngrok URL for websockets

const SocketEndpoint = 'http://6fe9d559.ngrok.io';

// adapted from tutorial on https://reactnavigation.org/docs/en/auth-flow.html

// App navigation and basic authentication flow

class SignInScreen extends React.Component {

  constructor(props) {
  	super(props);
	  this.state = {
	  	isConnected: false,
	  	email: 'u1@example.com',
	  	password: 'FakeTestPassword',
	  	correctLogin: false
	  };
  }


  static navigationOptions = {
    title: 'Please sign in',
  };

  componentDidMount() {

    // web socket ping
    const socket = io(SocketEndpoint, {
      transports: ['websocket'],
    });

    socket.on('connect', () => {
      this.state.isConnected = true;
    });
  }

  submitLogin() {

    //const ans = this.state.answer;

    // web socket ping
    const socket = io(SocketEndpoint, {
      transports: ['websocket'],
    });

    socket.on('connect', () => {
    	//
    });

    // placeholder authentication

    socket.emit('unsecurelogin', {
      email: this.state.email,
      password: this.state.password
    });

    //this.state.correctLogin = true;

    socket.on('correctlogin', () => {
    	this.state.correctLogin = true;
      console.log("correct");

      console.log(this.state.correctLogin);

      if (this.state.correctLogin == true) {
        this.props.navigation.navigate('App');
      }
    });

    
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.paragraph}>connected: {this.state.isConnected ? 'true' : 'false'}</Text>
      	<TextInput
          style={styles.input}
          onChangeText={(email) => this.setState({email})}
          defaultValue={this.state.email}
          autoCapitalize="none"
          autoCorrect={false}
        />

        <TextInput
          style={styles.input}
          onChangeText={(password) => this.setState({password})}
          defaultValue={this.state.password}
          autoCapitalize="none"
          autoCorrect={false}
          secureTextEntry={true}
        />
        <Button title="Sign in!" onPress={this.submitLogin.bind(this)} />
      </View>
    );
  }

  _signInAsync = async () => {
    await AsyncStorage.setItem('userToken', 'abc');
    this.submitLogin();
    if (correctLogin) {
    	this.props.navigation.navigate('App');
    }
  };
}

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Welcome to the app!',
  };

  render() {
    return (
      <View style={styles.container}>
        <Button title="Enter App" onPress={this._showMoreApp} />
        <Button title="Sign Out" onPress={this.signOut.bind(this)} />
      </View>
    );
  }

  _showMoreApp = () => {
    this.props.navigation.navigate('Main');
  };

  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  };

  signOut() {
    this.props.navigation.navigate('Auth');
  }
}

class OtherScreen extends React.Component {
  static navigationOptions = {
    title: 'Lots of features here',
  };

  render() {
    return (
      <View style={styles.container}>
        <Button title="I'm done, sign me out" onPress={this._signOutAsync} />
        <StatusBar barStyle="default" />
      </View>
    );
  }

  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  };
}

class AuthLoadingScreen extends React.Component {
  constructor() {
    super();
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userToken');

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(userToken ? 'App' : 'Auth');
  };

  // Render any loading content that you like here
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    width: 400,
    fontSize: 17,
    height: 36,
    padding: 10,
    backgroundColor: '#FFFFFF',
    borderColor: '#888888',
    borderWidth: 1,
    marginHorizontal: 20,
    marginBottom: 10,
  },
});

const AppStack = createStackNavigator({ Home: HomeScreen, Other: OtherScreen });
const AuthStack = createStackNavigator({ SignIn: SignInScreen });

export default createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
    Main: MainTabNavigator,
  },
  {
    initialRouteName: 'Auth',
  }
);
