import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import InfoScreen from '../screens/InfoScreen';
import QuizScreen from '../screens/QuizScreen';
import QuestionScreen from '../screens/QuestionScreen';

const InfoStack = createStackNavigator({
  Info: InfoScreen,
});

InfoStack.navigationOptions = {
  tabBarLabel: 'Attendance Check',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-cellular' : 'md-cellular'}
    />
    ),
};

const QuizStack = createStackNavigator({
  Quiz: QuizScreen,
});

QuizStack.navigationOptions = {
  tabBarLabel: 'Take Quiz',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-checkbox${focused ? '' : '-outline'}`
          : 'md-checkbox'
      }
    />
  ),
};

const QuestionStack = createStackNavigator({
  Question: QuestionScreen,
});

QuestionStack.navigationOptions = {
  tabBarLabel: 'Send Message',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-chatboxes' : 'md-chatboxes'}
    />
  ),
};

export default createBottomTabNavigator({
  InfoStack,
  QuizStack,
  QuestionStack,
});
