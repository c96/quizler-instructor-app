import React from 'react';
import {
  Button,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { Constants, Location, Permissions, WebBrowser } from 'expo';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';

const io = require('socket.io-client');

import { MonoText } from '../components/StyledText';

// Local ngrok URL for websockets

const SocketEndpoint = 'http://6fe9d559.ngrok.io';

var difference = function (a, b) { 
  return Math.abs(a - b); 
}

export default class InfoScreen extends React.Component {
  state = {
    isConnected: false,
    data: null,
    desiredLatitude: null,
    desiredLongitude: null,
    latValue: 0, // for location set
    lonValue: 0, // for location set
    locSet: "Waiting...",
    locCheckText: "Waiting...",
    checkLatValue: 0, // for location check
    checkLonValue: 0, // for location check
    numStudents: 0,
    locStatus: "--"
  };
  componentDidMount() {

    // web socket ping
    const socket = io(SocketEndpoint, {
      transports: ['websocket'],
    });

    socket.on('connect', () => {
      this.setState({ isConnected: true });
    });

    socket.on('ping', data => {
      this.setState(data);
    });

    socket.on('locate', data => {
      this.setState(desiredLatitude);
      this.setState(desiredLongitude);
    })

    // Android emulator check
    // Get location
    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({
        errorMessage: 'Error occurred, please try on Android device instead of emulator!',
      });
    } else {
      this._getLocationAsync();
    }
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    this.setState({ location });
  };

  pushLocation() {
    const socket = io(SocketEndpoint, {
      transports: ['websocket'],
    });

    socket.on('connect', () => {
      //
      console.log("connected for location sending");
    });

    // placeholder authentication

    socket.emit('setlocation', {
      lat: this.state.latValue,
      lon: this.state.lonValue
    });
  }

  setLocation() {


      this.state.locSet = // JSON.stringify(this.state.location) +
      "Lat: " + this.state.latValue.toFixed(4) 
      + "\nLon: " + this.state.lonValue.toFixed(4);
      this.pushLocation();
  }

  retrieveLocation() {

  }

  checkLocation() {

    const latValue = this.state.latValue;
    const lonValue = this.state.lonValue;
    const checkLatValue = this.state.checkLatValue;
    const checkLonValue = this.state.checkLonValue;
    const locStatus = this.state.locStatus;

    console.log(difference(latValue,checkLatValue));

      // check if attending through difference with default coords
      if (difference(latValue,checkLatValue) < 0.5
        && difference(lonValue,checkLonValue) < 0.5)
      {
        this.state.locStatus = "Location is close enough";
      }
      else {
        this.state.locStatus = "Location is not close enough"
      }

      this.state.locCheckText = // JSON.stringify(this.state.location) +
      "Lat: " + this.state.latValue.toFixed(4) 
      + "\nLon: " + this.state.lonValue.toFixed(4);
  }

  render() {

    let locText = 'Waiting...';

    // Set the text for the location set functions
    if (this.state.errorMessage) {
      locText = this.state.errorMessage;
      locStatus = this.state.errorMessage;
    } else if (this.state.location) {
      // lat set
      this.state.latValue = this.state.location.coords.latitude;
      this.state.lonValue = this.state.location.coords.longitude;

      // lat check
      this.state.checkLatValue = this.state.location.coords.latitude;
      this.state.checkLonValue = this.state.location.coords.longitude;

      locText = this.state.locSet;
    }

    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1, backgroundColor: '#605ca8'}} >
          <Image
            style={{flex: 1}}
            source={require('../assets/images/logo.png')}
            resizeMethod="resize"
            resizeMode="center"
          />
        </View>
        <View style={{flex: 5, padding: 20}}>
          <View style={{flex: 2, backgroundColor: '#ecf0f5', padding: 20}} >
            <Text>Connecting to Quizler...</Text>
            <Text style={styles.paragraph}>connected: {this.state.isConnected ? 'true' : 'false'}</Text>
            {this.state.data &&
              <Text>
                ping response: {this.state.data}
              </Text>}
          </View>
          <View style={{flex: 9, padding: 20, backgroundColor: '#f4f4f4'}} >
          <Text>Set Classroom Location</Text>
            <View style={{flex: 1, padding: 1, margin: 5, backgroundColor: '#e0e0f5'}}> 
            <Text style={styles.paragraph}>{locText}</Text>
            <Button
              onPress={this.setLocation.bind(this)}
              title="Set Location"
              color="red"
              accessibilityLabel="Set classroom location"
            />
            </View>

            <View style={{flex: 1, padding: 1, margin: 5, backgroundColor: '#e0e0f5'}}> 
            <Text style={styles.paragraph}>{this.state.locCheckText}</Text>
            <Button
              onPress={this.checkLocation.bind(this)}
              title="Check Location"
              color="red"
              accessibilityLabel="Check classroom location"
            />
            <Text style={styles.paragraph}>{this.state.locStatus}</Text>
            </View>
            <View style={{flex: 1, padding: 1, margin: 5, backgroundColor: '#e0e0f5'}}> 
              <Text style={styles.paragraph}>Students in Attendance</Text>
              <Text style={styles.paragraph}>{this.state.numStudents}</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 5,
    fontSize: 18,
    textAlign: 'center',
  },
});