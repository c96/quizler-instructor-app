import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { Constants, Location, Permissions, WebBrowser } from 'expo';

import { MonoText } from '../components/StyledText';

export default class QuestionScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {text: ''};
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1}}>
        </View>
        <View style={{flex: 1}}>
          <Text>Enter Question To Class Board</Text>
          <TextInput
            style={{height: 40}}
            placeholder="Submit Question to Instructor"
            onChangeText={(text) => this.setState({text})}
          />
        </View>
        <View style={{flex: 1}}>
        <Text>Formatted Question: {this.state.text}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    textAlign: 'center',
  },
});