## Quizler App

Mobile app for the Quizler system. The instructor app is in the quizler-instructor-app folder inside expo-apps.

The instructor app includes mockup authentication, location checking, receiving and answering quiz questions, using socket.io for the connection.

The Quizler app is built using Expo for iOS and Android. It has been tested and confirmed to work on iOS simulator, as well as iOS mobile device if tunnel connection is selected in Expo Developer Tools

### Components

* Mobile app (located in *expo-apps/quizler-instructor-app*) which is the instructor side app

   * Consists of an App Navigator with mockup authentication, sign in and sign out. Note that connection to the server is required to log in.
   
   * Multiple Screens located in a tab navigator:
   
      * InfoScreen for connecting to the server, and for setting and checking location.
      * QuizScreen for receiving quiz questions, sending the answer and validating whether it is correct.
      * QuestionScreen for submitting questions to the class board, this is partially implemented and requires further iteration.

* Server-side socket-tester (located in *socket-tester*) 

   * sqlite3 database running on node express sever.
   
   * requires ngrok to communicate with websockets to the mobile app over a tunnel.

### Installation

* Install Expo on mobile device using iOS App Store or Google Play Store.

* iOS simulator requires XCode.

* Install ngrok for testing the tunnel. https://ngrok.com/download


   git clone https://c96@bitbucket.org/c96/quizler-instructor-app.git

### Instructions

Summary: Start the node server, set up the ngrok tunnel, start Expo Tools, and connect mobile app to the tunnel using expo.

#### Step 1. Update and start the server

    cd socket-tester
    
    npm install
    
    node index.js
    
#### Step 2. Setup web sockets tunnel using ngrok

In the directory where ngrok is installed (or in any directory, if ngrok is in the path), enter:

    ngrok http 3000 

and copy the https url. That looks something like this: 

    https://9dbe0750.ngrok.io. 
    
For testing purposes, paste the generated URL from ngrok into the following files:

    expo-apps/quizler-instructor-client/screens/InfoScreen.js
    expo-apps/quizler-instructor-client/screens/QuizScreen.js 
    expo-apps/quizler-instructor-client/navigation/AppNavigator.js

and change the SocketEndpoint at the top of the file to point to your endpoint. 

This is required for using https with websockets.

#### Step 3. Start the expo developer tools 

Navigate to the app:

    cd expo-apps/quizler-instructor-client

Install expo-cli globally:

    npm install -g expo-cli
	
Install node packages:

    npm install

Start package bundler:

	expo start

Expo Developer Tools will open in the terminal and the browser.

#### Step 4. Start the mobile app

iOS device, iOS simulator, and Android device are supported. Android emulator is not supported.

##### Running on iOS device

In Expo Developer Tools, set Connection to Tunnel. 

Send SMS link using the option in Developer Tools. On the device, click the link in messages. 

This should build the app in Expo on the device.

##### Running in iOS simulator

To open the iOS simulator (requires XCode), enter in terminal:

    open -a Simulator

Press *i* in the terminal tab where Expo Tools is running to run the application in the iOS simulator

This will build the app in Expo and start on the iOS simulator.

##### Running on Android device

Similar to iOS device instructions. Send SMS link to the Android device, and set connection to Tunnel.

##### Running on Android emulator

Emulator not supported. It should run, but the location services do not work on the Android emulator.

### Additional 

#### Troubleshooting

Make sure that all node packages and expo are up to date in the */expo-apps/quizler-instructor-app* folder and */socket-tester* folder

If errors occur related to the iOS simulator, try the following:

1. Update XCode 

2. Manually open the iOS simulator using *open -a Simulator*, prior to starting it within Expo Tools

#### Images

![Screenshot-main](images/Screenshot-main.png)